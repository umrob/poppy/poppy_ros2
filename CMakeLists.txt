cmake_minimum_required(VERSION 3.5)
project(poppy_ros2)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_python REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclpy REQUIRED)
find_package(fmt REQUIRED)
find_package(std_msgs REQUIRED)
find_package(sensor_msgs REQUIRED)

add_executable(cpp_example src/cpp_example.cpp)
ament_target_dependencies(
  cpp_example
    rclcpp
    std_msgs
    sensor_msgs
)
target_link_libraries(cpp_example fmt::fmt)
target_compile_features(cpp_example PRIVATE cxx_std_17)

ament_python_install_package(${PROJECT_NAME})

install(TARGETS
  cpp_example
  DESTINATION lib/${PROJECT_NAME}
)

install(PROGRAMS
  scripts/py_example.py
  scripts/driver.py
  DESTINATION lib/${PROJECT_NAME}
)

install(DIRECTORY
  launch urdf meshes config
  DESTINATION share/${PROJECT_NAME}
)

ament_package()
