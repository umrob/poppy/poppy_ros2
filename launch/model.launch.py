import launch
from launch.substitutions import Command, LaunchConfiguration
from launch.actions import LogInfo
import launch_ros
import xacro
import os


def generate_launch_description():
    pkg_share = launch_ros.substitutions.FindPackageShare(
        package='poppy_ros2').find('poppy_ros2')
    model_path = os.path.join(pkg_share, 'urdf/poppy_humanoid.urdf.xacro')

    robot_state_publisher_node = launch_ros.actions.Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        parameters=[
            {'robot_description': Command([
                'xacro ', model_path,
                ' disable_gravity:=', LaunchConfiguration('disable_gravity'),
                ' robot_type:=', LaunchConfiguration('type')
            ])
            }]
    )

    return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument(name='type', default_value="humanoid",
                                             description='robot type, either humanoid (full robot) or arms (left and right arms only)'),
        launch.actions.DeclareLaunchArgument(name='disable_gravity', default_value="1",
                                             description='Enable or disable the gravity on Poppy\'s links'),
        robot_state_publisher_node
    ])
