from launch import LaunchDescription
import launch.actions
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, ExecuteProcess
from launch.launch_description_sources import PythonLaunchDescriptionSource
import launch_ros
import os


def generate_launch_description():
    pkg_share = os.path.join(
        get_package_share_directory('poppy_ros2'))

    sim = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'sim.launch.py')),
        launch_arguments={'type': 'humanoid'}.items()

    )

    return LaunchDescription([
        sim
    ])
