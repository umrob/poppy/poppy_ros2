from ament_index_python.packages import get_package_share_directory
import launch
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
import os


def generate_launch_description():
    pkg_share = os.path.join(
        get_package_share_directory('poppy_ros2'))

    model = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'model.launch.py')),
        launch_arguments={'type': LaunchConfiguration('type')}.items()
    )

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'gazebo.launch.py'))
    )

    return LaunchDescription([
        launch.actions.DeclareLaunchArgument(name='type', default_value="humanoid",
                                             description='robot type, either humanoid (full robot) or arms (left and right arms only)'),
        model,
        gazebo
    ])
