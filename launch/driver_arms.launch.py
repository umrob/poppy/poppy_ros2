import launch
import launch_ros


def generate_launch_description():
    poppy_driver_node = launch_ros.actions.Node(
        package='poppy_ros2',
        executable='driver.py',
        parameters=[
            {'type': 'arms'}]
    )

    return launch.LaunchDescription([
        poppy_driver_node
    ])
