#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from rcl_interfaces.srv import GetParameters


class PoppyController(Node):
    def __init__(self):
        super().__init__('poppy_controller')

        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/joint_group_position_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)

        self.get_joint_cmd_names()

        self.cmd_ = Float64MultiArray()
        for _ in range(self.joint_count()):
            self.cmd_.data.append(0)

        self.up = True

    def run(self):
        # The joint indicies might be different between the state vectors and the command vector
        r_elbow_y_state_idx = self.joint_state_index('r_elbow_y')
        r_elbow_y_cmd_idx = self.joint_cmd_index('r_elbow_y')

        if self.up:
            if self.joint_position[r_elbow_y_state_idx] < 1.57:
                self.cmd_.data[r_elbow_y_cmd_idx] = self.cmd_.data[r_elbow_y_cmd_idx] + 0.05
            else:
                self.up = False
        else:
            if self.joint_position[r_elbow_y_state_idx] > 0.0:
                self.cmd_.data[r_elbow_y_cmd_idx] = self.cmd_.data[r_elbow_y_cmd_idx] - 0.05
            else:
                self.up = True

        self.cmd_publisher_.publish(self.cmd_)

    def joint_state_callback(self, msg):
        # Start the timer on first joint state reception
        if not hasattr(self, "run_timer"):
            self.run_timer = self.create_timer(0.1, self.run)

        self.joint_state_names_ = msg.name
        self.joint_position = msg.position
        self.get_logger().info(
            "\nJoints\n\tname: %s\n\tposition: %s" %
            (msg.name, msg.position))

    def joint_state_index(self, joint_name):
        return self.joint_state_names_.index(joint_name)

    def joint_cmd_index(self, joint_name):
        return self.joint_cmd_names_.index(joint_name)

    def get_joint_cmd_names(self):
        self.cmd_params = self.create_client(
            GetParameters, '/joint_group_position_controller/get_parameters')
        self.cmd_params.wait_for_service()
        srv_params = GetParameters.Request()
        srv_params.names = ["joints"]
        resp_fut = self.cmd_params.call_async(srv_params)
        rclpy.spin_until_future_complete(self, resp_fut)
        self.joint_cmd_names_ = resp_fut.result().values[0].string_array_value

    def joint_count(self):
        return len(self.joint_cmd_names_)


def main(args=None):
    rclpy.init(args=args)

    poppy_controller = PoppyController()

    try:
        rclpy.spin(poppy_controller)
    except KeyboardInterrupt:
        pass
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
