#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState

import pypot.robot

import os
import yaml
import math


class PoppyDriver(Node):
    def __init__(self):
        super().__init__('poppy_driver')

        self.declare_parameter(
            "type", "robot type, either humanoid (full robot) or arms (left and right arms only)")

        self.robot_type_ = str(self.get_parameter("type").value)

        self.cmd_subscriber_ = self.create_subscription(
            Float64MultiArray, '/joint_group_position_controller/commands', self.joint_cmd_callback, 10)

        self.joint_state_publisher_ = self.create_publisher(
            JointState,
            '/joint_states',
            10)

        self.joint_state_ = JointState()

        # Use the poppy_controllers.yaml config file to get the order of the joints so that it's consistent with the simulated robot
        with open(os.path.join(
                get_package_share_directory("poppy_ros2"), "config", "poppy_%s_controllers.yaml" % self.robot_type_), "r") as stream:
            controller_config = yaml.safe_load(stream)
            self.joint_state_.name = controller_config[
                "joint_group_position_controller"]["ros__parameters"]["joints"]

        for _ in range(len(self.joint_state_.name)):
            self.joint_state_.position.append(math.nan)
            self.joint_state_.velocity.append(math.nan)
            self.joint_state_.effort.append(math.nan)

        self.poppy_ = pypot.robot.from_json(os.path.join(
            get_package_share_directory("poppy_ros2"), "config", "poppy_%s.json" % self.robot_type_))

        self.robot_initialized_ = True

        self.poppy_.start_sync()

        self.pub_timer = self.create_timer(0.02, self.publish_joint_state)

    def publish_joint_state(self):
        for idx, joint in enumerate(self.joint_state_.name):
            self.joint_state_.position[idx] = getattr(
                self.poppy_, joint).present_position * math.pi / 180.0

        self.joint_state_publisher_.publish(self.joint_state_)

    def joint_cmd_callback(self, msg):
        for idx, joint in enumerate(self.joint_state_.name):
            getattr(self.poppy_, joint).compliant = False
            getattr(self.poppy_,
                    joint).goal_position = msg.data[idx] * 180.0 / math.pi


def main(args=None):
    rclpy.init(args=args)

    poppy_driver = PoppyDriver()

    try:
        rclpy.spin(poppy_driver)
    except KeyboardInterrupt:
        pass
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
